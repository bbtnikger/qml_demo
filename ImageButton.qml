import QtQuick 2.5

Item {
    id: root

    property int buttonwidth: 40
    property int buttonheight: 40

    property string path

    signal clicked()

    width: buttonwidth
    height: buttonheight

    Image{
        id: image
        source: path
        width: buttonwidth-6
        height: buttonheight-6
        anchors.verticalCenter: parent.verticalCenter

        fillMode: Image.PreserveAspectFit

        Behavior on width{

            NumberAnimation{ duration: 80; easing.type: Easing.OutQuad }
        }

        Behavior on height{

            NumberAnimation{ duration: 80; easing.type: Easing.OutQuad }
        }
    }

    MouseArea{
        id: mouseArea
        anchors.fill: parent
        onPressed: {

            image.width = buttonwidth-9;
            image.height = buttonheight-9;
        }

        onReleased: {

            image.width = buttonwidth-6;
            image.height = buttonheight-6;
            root.clicked();

        }
    }
}

