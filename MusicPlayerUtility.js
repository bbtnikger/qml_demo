function play(){

    // play the audio
    audio.play();
}

function pause(){

    // pause the audio
    audio.pause();
}

function forward(){

    // firstly stop the current audio playing
    audio.stop();

    // check whether the currentIndex is smaller than the index of the last item on the model
    // if it is smaller advance the currentIndex by one
    if(currentIndex<songModel.count-1)
        currentIndex = currentIndex + 1;
    else // if it is not smaller then set the current index to zero, in other words start from the first song on the list
        currentIndex = 0;

    // start playing
    audio.play();
}

function rewind(){

    // if the position is greated than 1 second, then start playing the current song from the start
    if(audio.position/1000 > 1){

        // stop the audio
        audio.stop();

        // start playing
        audio.play();

    }else{

        // stop the audio
        audio.stop();

        // if the currentIndex is greater than zero then decrement the currentIndex by one
        if(currentIndex>0)
            currentIndex = currentIndex - 1;

        audio.play();
    }
}
