import QtQuick 2.5

ListModel {
    id: root

    ListElement{
        path: "music/01 Daydreamer.mp3"
        cover: "music/adele_19.png"
    }
    ListElement{
        path: "music/02 Go Ahead.mp3"
        cover: "music/alicia_keys_as_i_am.png"
    }
    ListElement{
        path: "music/02 Let's Get Excited.mp3"
        cover: "music/alesha_dixon_the_alesha_show.png"
    }
    ListElement{
        path: "music/03 Chasing Pavements.mp3"
        cover: "music/adele_19.png"
    }
    ListElement{
        path: "music/03 Superwoman.mp3"
        cover: "music/alicia_keys_as_i_am.png"
    }
    ListElement{
        path: "music/04 Cinderella Shoe.mp3"
        cover: "music/alesha_dixon_the_alesha_show.png"
    }
}

