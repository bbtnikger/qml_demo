import QtQuick 2.5

Item{
    id: root

    property int duration: 100000 // milliseconds
    property int position: 0 // milliseconds
    property int progress: 0 // milliseconds
    property bool seekable: true
    property bool behaviorOnXEnabled: false

    signal seek(int offset)

    function getMins(val){

        var res = val/60000;

        return parseInt(res);
    }

    function getSecs(val){

        var res = val%60000/1000;

        return parseInt(res);
    }

    function convertToTimeString(val){

        var timestring = val > 9? val : '0' + val;

        return timestring;
    }

    function calculateOffset(xpos){

        var offset = xpos/(background.width-knob.width/2)*duration;

//        console.log('offset:'+offset);

        return offset;
    }

    onPositionChanged: {

        var mins = getMins(position);
        var secs = getSecs(position);

        var minstring = convertToTimeString(mins);
        var secstring = convertToTimeString(secs);

        var timestring = minstring + ":" + secstring;
        textLeft.text = timestring;
    }

    onDurationChanged: {

        var mins = getMins(duration);
        var secs = getSecs(duration);

        var minstring = convertToTimeString(mins);
        var secstring = convertToTimeString(secs);

        var timestring = minstring + ":" + secstring;
        textRight.text = timestring;
    }

    onProgressChanged: {


    }

    Item{
        id: row
        anchors.fill: parent

        Text{
            id: textLeft
            text: "00:00"
            font { family: localFontExtraBold.name; pointSize: 16 }
            color: "#D1D1D1"
            anchors.verticalCenter: parent.verticalCenter
        }

        Item{
            id: positionSlider
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: textLeft.right
            height: background.height
            anchors.right: textRight.left
            anchors.leftMargin: 9
            anchors.rightMargin: 9

            BorderImage{
                id: background
                source: Qt.resolvedUrl("gfx/slider_background.png")
                width: parent.width
                border { left: 15; right: 15; }
                horizontalTileMode: BorderImage.Stretch
                verticalTileMode: BorderImage.Stretch
            }

            BorderImage{
                id: imageValueLeft
                source: Qt.resolvedUrl("gfx/slider_value_left.png")
                width: knob.x+knob.width/2
                border { left: 15; right: 15; }
                horizontalTileMode: BorderImage.Stretch
                verticalTileMode: BorderImage.Stretch
                anchors.verticalCenter: background.verticalCenter
            }

//            BorderImage{
//                id: imageValueRight
//                source: "gfx/slider_value_right.png"
//                x: knob.x+knob.width/2
//                border { left: 15; right: 15; }
//                horizontalTileMode: BorderImage.Stretch
//                verticalTileMode: BorderImage.Stretch
//                anchors.verticalCenter: background.verticalCenter
//            }


            DropArea{
                id: dropArea
                anchors.fill: background

                onDropped: {

//                    console.log('dropped')

                    var offset = calculateOffset(drop.x);

                    seek(offset);
                }
            }

            Image {
                id: knob
                source: dragArea.pressed? Qt.resolvedUrl("gfx/slider_knob_invert.png"):Qt.resolvedUrl("gfx/slider_knob.png")
                y: -6
                x: (background.width - width) * (position/duration)

                Behavior on x{
                    enabled: behaviorOnXEnabled
                    NumberAnimation{ duration: 1000 }
                }

                Drag.active: dragArea.drag.active
                Drag.hotSpot.x: width/2
                Drag.hotSpot.y: height/2

                MouseArea {
                    id: dragArea
                    anchors.fill: parent
                    enabled: seekable

                    drag.target: parent
                    drag.axis: Drag.XAxis
                    drag.minimumX: dropArea.x
                    drag.maximumX: dropArea.x+dropArea.width-width

                    onPressed: {

                        behaviorOnXEnabled = false;
                        audio.pause();
                    }

                    onReleased: {

                        audio.play();
                        behaviorOnXEnabled = true;
                        parent.Drag.drop();
                    }
                }
            }
        }


        Text{
            id: textRight
            text: "10:00"
            anchors.right: parent.right
            font { family: localFontExtraBold.name; pointSize: 16 }
            color: "#D1D1D1"
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}

