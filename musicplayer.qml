import QtQuick 2.5

Item {
    id: root

    width: backgroundImage.width
    height: backgroundImage.height

    Image{
        id: backgroundImage
        source: "gfx/background.png"
    }

    MusicPlayerBar{
        anchors.centerIn: parent
    }
}

