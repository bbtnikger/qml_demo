import QtQuick 2.5

Row {
    id: root

    signal playClicked()
    signal pauseClicked()
    signal rewindClicked()
    signal forwardClicked()

    property bool playing: false

    ImageButton{
        path: "icons/rewind.png"
        anchors.verticalCenter: parent.verticalCenter
        onClicked: {

            rewindClicked()
        }
    }

    ImageButton{
        visible: playing? false : true
        buttonwidth: 64
        buttonheight: 64
        anchors.verticalCenter: parent.verticalCenter
        path: "icons/play.png"
        onClicked: {

            playClicked()
        }
    }

    ImageButton{
        visible: playing? true: false
        buttonwidth: 64
        buttonheight: 64
        anchors.verticalCenter: parent.verticalCenter
        path: "icons/pause.png"
        onClicked: {

            pauseClicked()
        }
    }

    ImageButton{
        anchors.verticalCenter: parent.verticalCenter
        path: "icons/forward.png"
        onClicked: {

            forwardClicked()
        }
    }
}

