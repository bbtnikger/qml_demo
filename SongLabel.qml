import QtQuick 2.5

Item {
    id: root

    property string title: "Song Title"
    property string band: "Band Name"


    Text{
        text: title
        font { family: localFontBold.name; pointSize: 18 }
        anchors.top: parent.top
        color: "#D1D1D1"
    }

    Text{
        text: band
        font { family: localFontSemibold.name; pointSize: 16 }
        anchors.bottom: parent.bottom
        color: "lightblue"
    }
}

