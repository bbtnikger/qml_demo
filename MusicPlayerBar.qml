import QtQuick 2.5
import QtMultimedia 5.5
import "MusicPlayerUtility.js" as MusicPlayerUtility

Item {
    id: root
    width: image.width
    height: image.height

    // helper property used in the ButtonRow component
    property bool playing: false

    // the current index of the model, default value is -1
    property int currentIndex: -1

    // when the current index changes then set the audio source according to the model data
    // and also set the coverPhoto source according to the model data
    onCurrentIndexChanged: {

        console.log('Current Index Changed('+currentIndex+')');

        sliderItem.behaviorOnXEnabled = false;

        // extra checking
        if(currentIndex >= 0 && currentIndex < songModel.count){

            // set the audio source
            var audiosource = Qt.resolvedUrl(songModel.get(currentIndex).path);
            console.log('set the audio source ('+audiosource+')');
            audio.source = audiosource;

            // set the cover photo
            var coverphotosource = Qt.resolvedUrl(songModel.get(currentIndex).cover);
            console.log('set the cover photo source ('+coverphotosource+')');
            coverPhoto.source = coverphotosource;

        }else{

            console.log('This should ever not happen');
        }

        sliderItem.behaviorOnXEnabled = true;
    }

    // loaders for the local fonts provided
    // * not all of them are being used
    FontLoader { id: localFontBold; source: Qt.resolvedUrl("fonts/OpenSans-Bold.ttf") }
    FontLoader { id: localFontBoldItalic; source: Qt.resolvedUrl("fonts/OpenSans-BoldItalic.ttf") }
    FontLoader { id: localFontExtraBold; source: Qt.resolvedUrl("fonts/OpenSans-ExtraBold.ttf") }
    FontLoader { id: localFontExtraBoldItalic; source: Qt.resolvedUrl("fonts/OpenSans-ExtraBoldItalic.ttf") }
    FontLoader { id: localFontItalic; source: Qt.resolvedUrl("fonts/OpenSans-Italic.ttf") }
    FontLoader { id: localFontLight; source: Qt.resolvedUrl("fonts/OpenSans-Light.ttf") }
    FontLoader { id: localFontLightItalic; source: Qt.resolvedUrl("fonts/OpenSans-LightItalic.ttf") }
    FontLoader { id: localFontRegular; source: Qt.resolvedUrl("fonts/OpenSans-Regular.ttf") }
    FontLoader { id: localFontSemibold; source: Qt.resolvedUrl("fonts/OpenSans-Semibold.ttf") }
    FontLoader { id: localFontSemiboldItalic; source: Qt.resolvedUrl("fonts/OpenSans-SemiboldItalic.ttf") }

    // the background image for the music player bar
    Image{
        id: image
        source: Qt.resolvedUrl("gfx/bar.png")
    }

    // the model models the track urls and their cover photos
    SongModel{
        id: songModel
    }

    // the audio component
    Audio{
        id: audio

        onError: {

            console.log('[Audio error] code:'+error+',message:'+errorString);
        }

        metaData.onTitleChanged: {

            console.log('title changed');

            // get the metaData title
            var mtitle = metaData.title;
            console.log('get the metaData title('+mtitle+')');
            if(mtitle !== undefined)
                songLabel.title = mtitle;
            else
                console.log('Undefined MetaData Title')
        }

        metaData.onAuthorChanged: {

            console.log('author changed');

            // get the metaData author
            var mauthor = metaData.author;
            console.log('get the metaData author('+mauthor+')');
            if(mauthor !== undefined)
                songLabel.band = mauthor;
            else
                console.log('Undefined MetaData Author')
        }

        onStatusChanged: {

            console.log('Playback status changed ('+status+')');

            // when the audio has been loaded
            // set the song labels
            if(status == Audio.Loaded){

                console.log('Audio loaded');

            }else{

                // if the audio position has reached its maximum value
                if(status == Audio.EndOfMedia){

                    console.log('Audio End of media');

                    // forward to the next track
                    MusicPlayerUtility.forward();
                }
            }
        }

        onPlaybackStateChanged: {

            console.log('Playback state changed:'+playbackState);

            // if the playbackState is on a playing state then set the playing property to true
            if(playbackState == Audio.PlayingState)
                root.playing = true;
            else { // else set it to false

                root.playing = false;
            }
        }
    }

    // the photo cover item
    Item{
        id: songCover
        anchors.left: parent.left
        anchors.leftMargin: 32
        anchors.verticalCenter: parent.verticalCenter
        width: 124
        height: 124

        Image{
            id: frame
            source: Qt.resolvedUrl("gfx/cover_frame.png")
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }

        Image{
            id: coverPhoto
            anchors.fill: parent
            anchors.margins: 8 // set its margin to 8 in order to make the frame visible at the borders
            fillMode: Image.PreserveAspectFit

            onStatusChanged: {

                console.log('Image status('+status+')');
            }
        }

        Image{
            id: overlay
            anchors.fill: parent
            anchors.margins: 6
            opacity: 0.9
            source: Qt.resolvedUrl("gfx/cover_overlay.png")
        }
    }

    // the right part of the music player bar right next to the cover photo
    Item{
        height: songCover.height
        anchors.left: songCover.right
        anchors.right: parent.right
        anchors.leftMargin: 9
        anchors.rightMargin: 32
        anchors.verticalCenter: parent.verticalCenter

        // a row of playback buttons
        ButtonRow{
            id: row
            spacing: 20
            height: parent.height * .6
            playing: root.playing
            onPlayClicked: MusicPlayerUtility.play()
            onPauseClicked: MusicPlayerUtility.pause()
            onRewindClicked: MusicPlayerUtility.rewind()
            onForwardClicked: MusicPlayerUtility.forward()

        }

        // A display frame of the current track playing
        SongLabel{
            id: songLabel
            anchors.left: row.right
            anchors.leftMargin: 20
            anchors.right: shareImageButton.left
            anchors.rightMargin: 20
            anchors.top: row.top
            anchors.bottom: row.bottom
        }

        // a share button
        ImageButton{
            id: shareImageButton
            anchors.right: parent.right
            path: Qt.resolvedUrl("icons/share.png")
        }

        // The Slider component
        Slider{
            id: sliderItem
            anchors.left: parent.left
            anchors.right: shareImageButton.right
            anchors.top: row.bottom
            anchors.bottom: parent.bottom
            anchors.topMargin: 9
            duration: audio.duration
            position: audio.position
//            seekable: audio.seekable
            onSeek: {

                audio.seek(offset);
            }
        }
    }

    Component.onCompleted: {

        // at startup set the current index to 0 if the song model is not empty
        if(songModel.count>0){

            currentIndex = 0;
        }
    }
}

